package main

import "fmt"

func main() {

	//Declaring variables
	/*
		var i int
		var s string
	*/

	// Declaring variables
	/*
		var i = 10
		var s = "Canada"
	*/

	//Declaration with initialization
	var i int = 10
	var s string = "Canada"

	fmt.Println(i)
	fmt.Print(s)
}
