package main

import (
	"fmt"
	"reflect"
)

func main() {

	/*
		You can omit the variable type
		from the declaration, when you
		are assigning a value to a
		variable at the time of declaration.
		The type of the value assigned to
		the variable will be used as the type
		of that variable.
	*/
	var i = 10
	var s = "Canada"

	fmt.Println(reflect.TypeOf(i))
	fmt.Println(reflect.TypeOf(s))
}
