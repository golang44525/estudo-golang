package main

import "fmt"

/*
In the Go language, the scope of
a variable refers to the region of
the code where the variable is
accessible. The scope of a variable
is determined by where it is declared.

A variable declared within a block
(e.g., inside a function or a control
structure such as if, for, or switch)
is only accessible within that block
and any nested blocks. Once the execution
leaves the block, the variable goes out
of scope and cannot be accessed anymore.

*/

var country = "Japan"

func main() {

	fmt.Println("Hello, ", country)
	x := true

	// If 'x' is true, execute the block of code inside the if statemen
	if x {

		/*
			// Declaring and initializing a
			local variable 'y' inside the if
			block
		*/

		y := 1 // Declaring and initializing a local variable 'y' inside the if block

		if x != false { // If 'x' is not false, execute the block of code inside the if statement
			fmt.Println(country) // Accessing the global variable 's' and printing its value
			fmt.Println(x)       // Printing the value of local variable 'x'
			fmt.Println(y)       // Printing the value of local variable 'y'
		}

	}

	fmt.Println(x) // Printing the value of local variable 'x'
}
