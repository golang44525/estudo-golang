package main

import "fmt"

func main() {

	/*
		Golang allows you to assign values
		to multiple variables in one line.
	*/
	var fname, lname string = "John", "Doe"
	m, n, o := 1, 2, 3
	item, price := "Mobile", 200

	fmt.Println(fname + lname)
	fmt.Println(m + n + o)
	fmt.Println(item, "-", price)
}
