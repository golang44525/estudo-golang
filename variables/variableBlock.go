package main

import "fmt"

//Variables declaration can be grouped together into blocks for greater readability and code quality.

var (
	product  = "Mobile"
	quantity = 50
	price    = 50.50
	inSock   = true
)

func main() {

	fmt.Println(product)
	fmt.Println(quantity)
	fmt.Println(price)
	fmt.Println(inSock)
}
