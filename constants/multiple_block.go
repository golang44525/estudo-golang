package main

import "fmt"

const (
	PRODUCT  = "Mobile"
	QUANTITY = 60
	PRICE    = 50.50
	STOCK    = true
)

func main() {

	fmt.Println(PRODUCT)
	fmt.Println(PRICE)
	fmt.Println(QUANTITY)
	fmt.Println(STOCK)
}
