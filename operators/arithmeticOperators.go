package main

import "fmt"

func main() {

	var x, y int = 35, 7

	fmt.Println("x + y = %d", x+y)
	fmt.Println("x - y = %d", x-y)
	fmt.Println("x * y = %d", x*y)
	fmt.Println("x mod y = %d", x%y)

	x++
	fmt.Println("x++ = ", x)

	y--
	fmt.Println("y-- = ", y)
}
